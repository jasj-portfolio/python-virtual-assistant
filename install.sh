#!/usr/bin/env sh

echo "Do you wish to remove dependency github files after installation?"
read githhub

cd dependencies

git clone https://github.com/NeuralNine/neuralintents.git
cd neuralintents
python setup.py install
cd ..

git clone https://github.com/sloria/TextBlob.git
cd TextBlob
python setup.py install
cd ..

git clone https://github.com/psf/requests.git
cd requests
python setup.py install
cd ..

git clone https://github.com/nltk/nltk.git
cd nltk
python setup.py install
cd ..

cd ..

if [ $github = "yes" ]
then
    rm -rf dependencies
else
    :
fi

python3 -c import nltk nltk.download("All")

echo "Done"