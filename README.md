# Python Virtual Assistant

A virtual assistant I have been working on to help with tasks in my home lab.

# Project is currently broken. I am working on it.
NeuralIntents has finally seen an update so I should have it working soon.

## Name
     ____        ____        _   
    |  _ \ _   _| __ )  ___ | |_ 
    | |_) | | | |  _ \ / _ \| __|
    |  __/| |_| | |_) | (_) | |_ 
    |_|    \__, |____/ \___/ \__|
           |___/       

## Description
A virtual assistant I am working on to help me automate tasks in my home lab. This is a personal project so maintence will be slow and updates will not be frequent. With that being said I hope you enjoy this little project of mine. Nueralintents seems to be dead with no updates in the last couple years so I plan on forking it and modifying for this project. With that being said updates will still be slow.

## Installation
- git clone https://gitlab.com/jasj-portfolio/python-virtual-assistant.git
- cd python-virtual-assistant
- chmod +x install.sh
- ./install.sh

## Usage
As an open source chat bot that can perform tasks on my network my assistant will eventually replace my google assistant and alexa. The chat bot needs to be run from the same directory everytime, if you move main.py you will need to move all files with it. Working on fixing this in version 2.

## Contributing
I would like to have tons of unique skills for this skills are pretty easy to write. Currently skills are written as simple functions in python and saved in the skills folder. If anyone writes any cool skills I would love to add them to the repo and give the author full credit.
