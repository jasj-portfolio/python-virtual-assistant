#1/usr/bin/env python3
import os

path = ''

def todo_add():
    
    try:
        print("What would you like add to the list?")
        messages = input("?>> ")
        todo = open("todo.txt", "a")
        todo.write(f"* {messages} \n")
        todo.close()
        print(f"{messages} was added to the list.")
    except:
        print("error unable to add activity")
    
def todo_remove():
    
    try:
        print("What is a keyword unique to the activity you wish to remove?")
        messages = input("?>> ")
        try:
            with open('todo.txt', 'r') as fr:
                lines = fr.readlines()
                with open('todo1.txt', 'w') as fw:
                    for line in lines:
                        if line.find(messages) == -1:
                            fw.write(line)
            print("Activity was deleted")
            os.replace("todo1.txt", "todo.txt")
        except:
            print("Was unable to delete activity")
    except:
        print("error unable to remove from list")
            
def show_todo():
    lines = []
    print("Items are in the order they were added.\n" + "Here's what needs to be done...")
    with open('todo.txt') as f:
        lines = f.readlines()
    for line in lines:
        print(line)
