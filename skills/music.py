#!/usr/bin/env python3
import os

def music_player():
    
    try:
        print("Which playlist would you like to listen to?\n note: Give PyBot the full path")
        messages = input("?>> ")
        playlist = f"{messages}"
        media_player = vlc.MediaPlayer()
        media = vlc.Media(playlist)
        media_player.set_media(media)
        media_player.play()
        time.sleep(5)

        a = False

        while a != True:
            b = str(input("please type pause, play, or stop: "))
            if b.lower() == "pause":
                media_player.pause()
            elif b.lower() == "stop":
                media_player.stop()
                a = True
            elif b.lower() == "play":
                media_player.play()
            else:
                print("Please choose play, pause, or stop")
    except:
        print("error unable to play playlist.")
