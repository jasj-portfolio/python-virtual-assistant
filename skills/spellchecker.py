#!/usr/bin/env python3
from textblob import TextBlob

def spellcheck():
    
    try:
        print("What word do you need help with?")
        messages = input("?>> ")
        b = TextBlob(messages)
        c = str(b.correct())
        if b.lower() == c.lower():
            print("You spelled it correctly.")
        else:
            print(c)
    except:
        print("error unable to get spelling.")
