from neuralintents import BasicAssistant
import json
from os.path import exists
from sys import exit
from skills import *


# You can change the name of PyBot here if you wish.
name = "pybot"

def your_name():
    print(f'Hello my name is {name}')

# Mappings is a dictionary that connects your intents file with the appropriate skill.
# The way mine is set up I have all my skills in a folder and I imported the folder.
# The syntax for mapping a skill is as follows,
# "<name of intent in intents file>" : <skill file>.<skill function>
# Leave it to one skill per line so you are able to comment out skills you want to temporarily turn off.
mappings = {
            'name': your_name,
            'weather' : weather.weather,  
            'date' : calendar.dates, 
            'todo_add' : todo.todo_add, 
            'todo_remove' : todo.todo_remove, 
            'todo' : todo.show_todo, 
            'spellcheck' : spellchecker.spellcheck, 
            'music' : music.music_player,  
            }

assistant = BasicAssistant('intents.json', method_mappings=mappings, model_name=name)
#assistant.fit_model()
#assistant.save_model()

# Checks if you already have a training model
if exists(f"{name}.keras") == True:
    assistant.load_model()
elif exists(f"{name}.") == False:
    assistant.fit_model(epochs=200)
    assistant.save_model()
else:
    print("Something went wrong...")


print("""
          +================================+
          #   ____        ____        _    #
          #  |  _ \ _   _| __ )  ___ | |_  #
          #  | |_) | | | |  _ \ / _ \| __| #
          #  |  __/| |_| | |_) | (_) | |_  #
          #  |_|    \__, |____/ \___/ \__| #
          #         |___/                  #
          +================================+
    To quit talking to PyBot type "q" or "quit". Ver 1.7
""")

while True:
    try:
        messages = input(">> ")
        if messages.lower() == "quit" or messages.lower() == "q":
            with open('encounters.txt', 'a') as e:
                goodbye = e.write('* \n')
            print("Goodbye.")
            break
        else:
            print(assistant.process_input(messages))
            print('\n') 
    except:
        print("something has gone wrong")

exit()
